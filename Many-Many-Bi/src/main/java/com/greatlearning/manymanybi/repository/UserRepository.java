package com.greatlearning.manymanybi.repository;

import com.greatlearning.manymanybi.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

}
