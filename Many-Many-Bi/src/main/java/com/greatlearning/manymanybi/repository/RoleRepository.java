package com.greatlearning.manymanybi.repository;

import com.greatlearning.manymanybi.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {
}
